#!/bin/sh

case "$1" in
    --toggle)
        if [ "$(pgrep pcloud)" ]; then
            pkill -f pcloud
        else
            gtk-launch appimagekit-pcloud &
        fi
        ;;
    *)
        if [ "$(pgrep pcloud)" ]; then
            echo " "
        else
            echo " "
        fi
        ;;
esac
